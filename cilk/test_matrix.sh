# matrix A times 0 matrix
CILK_NWORKERS=4 ./matrix 10 matrix_A.txt matrix_0.txt > result.txt 
./compare 10 result_A_0.txt result.txt 
# matrix A times identity matrix
CILK_NWORKERS=4 ./matrix 10 matrix_A.txt matrix_I.txt > result.txt
./compare 10 result_A_I.txt result.txt 
# identity matrix time identity matrix
CILK_NWORKERS=4 ./matrix 10 matrix_I.txt matrix_I.txt > result.txt
./compare 10 result_I_I.txt result.txt 
# matrix A times matrix A
CILK_NWORKERS=4 ./matrix 10 matrix_A.txt matrix_A.txt > result.txt
./compare 10 result_A_A.txt result.txt 
