# Test 1
mpiexec -np 64 -bind-to core ./jacobi 4 4 4 100
# Test 2
mpiexec -np 8 -bind-to core ./jacobi 2 2 2 10
# Test 3
mpiexec -np 16 -bind-to core ./jacobi 2 2 4 20
# Test 4
mpiexec -np 32 -bind-to core ./jacobi 2 2 4 40
