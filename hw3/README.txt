Costa Rica Institute of Technology
School of Computing
Esteban Meneses, PhD

This directory contains the following files:
* Makefile: to build the code
* io.h: input/output routines 
* shear.cpp: skeleton file for parallel code of shear sort 
* shear.pbs: script file for batch processing of parallel code of shear sort
* result_X_Y: result file for test Y of program X
* test_X_Y: test case Y for program X
* timer.h: timing routines 
