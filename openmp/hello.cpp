/**
 * Costa Rica Institute of Technology
 * School of Computing
 * Parallel Computing (MC-8836)
 * Instructor Esteban Meneses, PhD (esteban.meneses@acm.org)
 * OpenMP Hello World
 */

#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <omp.h>
#include <math.h>

using namespace std;

// Main method      
int main(int argc, char* argv[]) {

	int id, np;
	#pragma omp parallel private(id, np)
	{
		np = omp_get_num_threads();
		id = omp_get_thread_num();	
		cout << "Hello from thread: " << id << " out of " << np << " threads\n" << std::endl;
	}
	return 0;	
}

