# matrix A times zero matrix
CILK_NWORKERS=4 ./strassen 10 matrix_A.txt matrix_0.txt > result.txt 
./compare 10 result_strassen_A_0.txt result.txt
# matrix A times identity matrix
CILK_NWORKERS=4 ./strassen 10 matrix_A.txt matrix_I.txt > result.txt
./compare 10 result_strassen_A_I.txt result.txt
# identity matrix time identity matrix
CILK_NWORKERS=4 ./strassen 10 matrix_I.txt matrix_I.txt > result.txt
./compare 10 result_strassen_I_I.txt result.txt
# matrix A times matrix A
CILK_NWORKERS=4 ./strassen 10 matrix_A.txt matrix_A.txt > result.txt
./compare 10 result_strassen_A_A.txt result.txt
# zero matrix times identity matrix 
CILK_NWORKERS=4 ./strassen 10 matrix_0.txt matrix_I.txt > result.txt
./compare 10 result_strassen_0_I.txt result.txt
# matrix B times matrix C
CILK_NWORKERS=4 ./strassen 100 matrix_B.txt matrix_C.txt > result.txt
./compare 100 result_strassen_B_C.txt result.txt
# matrix B times matrix C
CILK_NWORKERS=16 ./strassen 100 matrix_B.txt matrix_C.txt > result.txt
./compare 100 result_strassen_B_C.txt result.txt
# matrix C times matrix B
CILK_NWORKERS=16 ./strassen 100 matrix_C.txt matrix_B.txt > result.txt
./compare 100 result_strassen_C_B.txt result.txt
# matrix U times matrix U
CILK_NWORKERS=16 ./strassen 100 matrix_U.txt matrix_U.txt > result.txt
./compare 100 result_strassen_U_U.txt result.txt
# matrix L times matrix L
CILK_NWORKERS=16 ./strassen 100 matrix_L.txt matrix_L.txt > result.txt
./compare 100 result_strassen_L_L.txt result.txt

